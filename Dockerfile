FROM ubuntu

COPY ./composer.json /
COPY ./entrypoint.sh /
COPY ./provision.sh /

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y tzdata
RUN chmod +x ./provision.sh
RUN ./provision.sh

ENTRYPOINT ["/entrypoint.sh"]